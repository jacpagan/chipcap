#include <CFF_ChipCap2.h>

#include <Wire.h>



/**************************************************************************
    This is a test for the CFF_ChipCap2 Library
    
    This test will put the ChipCap2 Sensor into Command Mode,
    Set a High and Low Alarm Value,
    Then go back into Normal Mode and then print Temperature and Humidity
    every 5 seconds.
***************************************************************************/


#include <DS1307RTC.h>

#include <Time.h>
#define TIME_MSG_LEN  11   // time sync to PC is HEADER followed by Unix time_t as ten ASCII digits
#define TIME_HEADER  'T'   // Header tag for serial time sync message
#define TIME_REQUEST  7    // ASCII bell character requests a time sync message 


CFF_ChipCap2 cc2 = CFF_ChipCap2();

unsigned long time;

void setup()
{
  
  Serial.begin(9600);
  setSyncProvider(RTC.get);
  if(Serial.available()) 
    {
        processSyncMessage();
    }
  cc2.begin();

  
  cc2.startCommandMode();
  delay(100);
  if (cc2.status == CCF_CHIPCAP2_STATUS_COMMANDMODE)
  {
    Serial.print("ChipCap2 is now in command mode!\n"); 
    Serial.print("Setting New Alarm Values\n"); 
    // Set Alarm Registers
    cc2.setAlarmHighVal(29);
    delay(100);
    cc2.setAlarmLowVal(18);
    delay(100);
    
    Serial.print("Going back into Normal mode\n"); 
    // Go back to normal mode
    cc2.startNormalMode();
    delay(100);
  }
  else
  {
     Serial.print("ChipCap2 is in normal mode."); 
     Serial.println();  }
  delay(2);
  cc2.configReadyPin(2);
  cc2.configAlarmLowPin(3);
  cc2.configAlarmHighPin(4);

}

void loop()
{
  
  while (1)
  {
    
    if (cc2.dataReady() == true)
    {
      
      cc2.readSensor();

      Serial.print("Time: ");

      if(timeStatus() != timeNotSet) 
      {
        digitalWrite(13,timeStatus() == timeSet);
        digitalClockDisplay();
      }
      Serial.print(" ");
      
      Serial.print("Humidity: ");
      Serial.print(cc2.humidity);
      Serial.print(" ");
  
      Serial.print("Temperature C: ");
      Serial.print(cc2.temperatureC);
      Serial.println();
      
      delay(6000);
    }
  }
}

void digitalClockDisplay(){
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(day());
  Serial.print(" ");
  Serial.print(month());
  Serial.print(" ");
  Serial.print(year()); 
}

void printDigits(int digits){
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void processSyncMessage() {
  // if time sync available from serial port, update time and return true
  while(Serial.available() >=  TIME_MSG_LEN ){  // time message consists of a header and ten ascii digits
    char c = Serial.read() ; 
    Serial.print(c);  
    if( c == TIME_HEADER ) {       
      time_t pctime = 0;
      for(int i=0; i < TIME_MSG_LEN -1; i++){   
        c = Serial.read();          
        if( c >= '0' && c <= '9'){   
          pctime = (10 * pctime) + (c - '0') ; // convert digits to a number    
        }
      }   
      setTime(pctime);   // Sync Arduino clock to the time received on the serial port
    }  
  }
}

time_t requestSync()
{
  Serial.write(TIME_REQUEST);  
  return 0; // the time will be sent later in response to serial mesg
}



